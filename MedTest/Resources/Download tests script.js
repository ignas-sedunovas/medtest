var noImage = 'Paveiksliukas neegzistuoja!';
function encodeImage(url, callback) {
    var image = new Image();
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;
        canvas.getContext('2d').drawImage(this, 0, 0);
        callback(canvas.toDataURL('image/jpeg', 0.5));
    }
    image.onerror = function () {
        callback(noImage);
    }
    image.src = url;
}
function downloadAsFile(html, fileName) {
    console.log('downloading', fileName);
    var content = $('<div></div>').html(html).html();
    var blob = new Blob([content]);
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = fileName;
    link.click();
}
var items = $('#KategorijaLt_listbox').data().kendoStaticList.dataItems();
console.log('total items to download', items.length);
for (var i = 0; i < items.length; i++) {
    $.ajax({
        url: 'https://lsmusis.lsmuni.lt/Klausimai/Index?KategorijaId=' + items[i].Value,
        context: {
            fileName: items[i].Text + '.html'
        }
    }).done(function (result) {
        console.log('content received', this.fileName);
        var that = this;
        var html = $(result);
        var images = html.find('img');
        var imageCount = images.length;
        if (imageCount === 0) {
            downloadAsFile(html, that.fileName);
        } else {
            images.each(function (i, image) {
                encodeImage(image.src, function (newSrc) {
                    if (newSrc === noImage) {
                        $(image).attr('alt', noImage);
                    } else {
                        $(image).attr('src', newSrc).removeAttr("style");
                    }
                    imageCount--;
                    if (imageCount === 0) {
                        downloadAsFile(html, that.fileName);
                    }
                });
            });
        }
    });
}
