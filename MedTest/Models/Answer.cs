﻿using System.Collections.Generic;

namespace MedTest.Models
{
    public class Answer
    {
        public string Single { get; set; }

        public List<string> Multiple { get; set; }
    }
}
