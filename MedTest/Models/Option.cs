﻿namespace MedTest.Models
{
    public class Option
    {
        public string Number { get; set; }

        public string Text { get; set; }
    }
}
