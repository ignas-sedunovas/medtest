﻿namespace MedTest.Models
{
    public enum QuestionType
    {
        Single1 = 1,
        Match = 2,
        Multi = 3,
        Single2 = 4
    }
}
