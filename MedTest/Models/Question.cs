﻿using System.Collections.Generic;

namespace MedTest.Models
{
    public class Question
    {
        public Question()
        {
            Options = new List<Option>();
            SecondaryOptions = new List<Option>();
        }

        public string CategoryName { get; set; }

        public string Number { get; set; }

        public string Text { get; set; }

        public List<Option> Options { get; set; }

        public List<Option> SecondaryOptions { get; set; }
    }
}
