﻿using System.Collections.Generic;

namespace MedTest.Models
{
    public class Category
    {
        public Category()
        {
            SubCategories = new List<SubCategory>();
            Answers = new Dictionary<string, Answer>();
        }

        public string Name { get; set; }

        public List<SubCategory> SubCategories { get; set; }

        public Dictionary<string, Answer> Answers { get; set; }
    }
}
