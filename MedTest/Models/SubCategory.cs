﻿using System.Collections.Generic;

namespace MedTest.Models
{
    public class SubCategory
    {
        public SubCategory()
        {
            Questions = new List<Question>();
        }

        public string Text { get; set; }

        public QuestionType QuestionType { get; set; }

        public List<Question> Questions { get; set; }
    }
}
