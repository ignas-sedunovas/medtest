﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using HtmlAgilityPack;
using MedTest.Models;
using Newtonsoft.Json;

namespace MedTest
{
    public partial class Form1 : Form
    {
        private const string TemplatesFolderPath = @"..\..\Resources\";
        private const string TestFileFolderPath = @"..\..\testai\";
        private const string OutputFolderPath = @"..\..\Output\";

        private const string TestTemplateFileName = "Page.html";
        private const string CategoriesFileName = "categories.js";
        private const string JQueryFileName = "jquery-3.2.1.min.js";

        private Dictionary<string, Category> categories = new Dictionary<string, Category>();

        public Form1()
        {
            InitializeComponent();
        }

        private void ParseFiles()
        {
            foreach (var fileName in Directory.EnumerateFiles(TestFileFolderPath))
            {
                var file = File.OpenText(fileName);
                var content = file.ReadToEnd();

                var category = new Category();

                ProcessCategory(category, content, Path.GetFileNameWithoutExtension(fileName));

                categories.Add(category.Name, category);
            }
        }

        private void ProcessCategory(Category category, string content, string fileName)
        {
            var questionTypeMap = new Dictionary<string, QuestionType>();

            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(content.Trim());

            var categoryNameNode = doc.DocumentNode.FirstChild;
            category.Name = categoryNameNode.GetAttributeValue("class", null) == "Centre Kategorija" ? categoryNameNode.InnerText : fileName;

            SubCategory subCategory = null;

            foreach (var node in doc.DocumentNode.ChildNodes)
            {
                if (node.GetAttributeValue("class", null) == "SubKategorija")
                {
                    subCategory = CreateSubCategory(fileName, node);

                    category.SubCategories.Add(subCategory);

                    continue;
                }

                if (node.Name == "ul")
                {
                    Debug.Assert(subCategory != null, $"subCategory is null, {fileName}");

                    subCategory.Questions.Add(ProcessQuestion(fileName, node, category.Name, subCategory, questionTypeMap));

                    continue;
                }

                if (node.Name == "div" && node.GetAttributeValue("class", null) == "Answers")
                {
                    ProcessAnswers(fileName, category, node.ChildNodes["table"], questionTypeMap);
                }
            }

            foreach (var subCat in category.SubCategories)
            {
                subCat.Questions = subCat.Questions.OrderBy(x => int.Parse(x.Number.Split('.')[1])).ToList();
            }

            category.SubCategories = category.SubCategories.OrderBy(x => x.QuestionType).ToList();
        }

        private static SubCategory CreateSubCategory(string fileName, HtmlNode subCategoryNode)
        {
            var subCategory = new SubCategory();

            switch (subCategoryNode.ChildNodes["span"].InnerText)
            {
                case "I tipo klausimai.":
                    subCategory.QuestionType = QuestionType.Single1;
                    break;
                case "II tipo klausimai.":
                    subCategory.QuestionType = QuestionType.Match;
                    break;
                case "III tipo klausimai.":
                    subCategory.QuestionType = QuestionType.Multi;
                    break;
                case "IV tipo klausimai.":
                    subCategory.QuestionType = QuestionType.Single2;
                    break;
                default:
                    throw new NotImplementedException($"Question type, {fileName}");
            }

            subCategory.Text = subCategoryNode.InnerHtml;

            return subCategory;
        }

        private static Question ProcessQuestion(string fileName, HtmlNode questionNode, string categoryName, SubCategory subCategory, Dictionary<string, QuestionType> questionTypeMap)
        {
            var questionParts = questionNode.SelectNodes("li").ToList();
            var question = new Question();

            switch (subCategory.QuestionType)
            {
                case QuestionType.Single1:
                case QuestionType.Single2:
                case QuestionType.Multi:
                    Debug.Assert(questionParts.Count == 2, $"too many question parts, {questionParts[0].InnerText}, {fileName}");

                    var optionNodes = questionParts[1].SelectNodes("ul//li").ToList();
                    if (subCategory.QuestionType == QuestionType.Multi)
                    {
                        //Debug.Assert(optionNodes.Count == 4, $"incorrect multi option count {optionNodes.Count}, {questionParts[1].InnerText}, {fileName}");
                        Debug.Assert(optionNodes.Count >= 4, $"incorrect multi option count {optionNodes.Count}, {questionParts[1].InnerText}, {fileName}");
                    }

                    ProcessOptions(fileName, optionNodes, question.Options, questionParts[0].ChildNodes["b"].InnerText.Trim());

                    if (subCategory.QuestionType == QuestionType.Multi)
                    {
                        Debug.Assert(string.Join(",", question.Options.Select(x => x.Number)) == string.Join(",", question.Options.Select(x => x.Number).OrderBy(x => x)),
                            $"incorrect multi option order {optionNodes.Count}, {questionParts[0].InnerText}, {fileName}");
                    }

                    break;
                case QuestionType.Match:
                    Debug.Assert(questionParts.Count == 3, $"too many question parts, {questionParts[0].InnerText}, {fileName}");

                    var firstOptionNodes = questionParts[1].SelectNodes("ul//li").ToList();
                    var secondaryOptionNodes = questionParts[2].SelectNodes("ul//li").ToList();
                    // Valid case:
                    // Debug.Assert(firstOptionNodes.Count == secondaryOptionNodes.Count, $"option count mismatch {firstOptionNodes.Count} <> {secondaryOptionNodes.Count}, {questionParts[1].InnerText}, {questionParts[2].InnerText}, {fileName}");

                    ProcessOptions(fileName, firstOptionNodes, question.Options, questionParts[0].ChildNodes["b"].InnerText.Trim());
                    ProcessOptions(fileName, secondaryOptionNodes, question.SecondaryOptions, questionParts[0].ChildNodes["b"].InnerText.Trim());

                    break;
                default:
                    throw new NotImplementedException($"Question type, {fileName}");
            }

            question.CategoryName = categoryName;
            question.Number = questionParts[0].ChildNodes["b"].InnerText.Trim();
            question.Text = questionParts[0].ChildNodes[1].InnerHtml.Trim() + questionParts[1].SelectSingleNode("div//img")?.OuterHtml.Replace("src=\"/Klausimai/", "src=\"https://lsmusis.lsmuni.lt/Klausimai/");

            questionTypeMap.Add(question.Number, subCategory.QuestionType);

            return question;
        }

        private static void ProcessOptions(string fileName, List<HtmlNode> optionNodes, List<Option> options, string questionNumber)
        {
            var idx = 1;
            foreach (var optionNode in optionNodes)
            {
                Debug.Assert(optionNode.InnerText == optionNode.InnerHtml, $"option contains html, \"{optionNode.InnerText}\", {fileName}");

                var optionContent = optionNode.InnerText.Trim();
                var parts = Regex.Match(optionContent, @"([A-Z]|\d+)?[ \.\-]+(.*)").Groups;

                Debug.Assert(parts.Count == 3, $"invalid option number format, {optionContent}, {questionNumber}, {fileName}");

                var option = new Option();
                if (!string.IsNullOrEmpty(parts[1].ToString()))
                {
                    option.Number = parts[1].ToString();
                    option.Text = parts[2].ToString();
                    //Debug.Assert(option.Number.Equals(idx.ToString()) || option.Number.Equals(((char)('A' + idx - 1)).ToString()), $"invalid option order, {option.Number} != {idx} || {(char)('A' + idx - 1)}, {questionNumber}, {fileName}");
                }
                else
                {
                    option.Number = idx.ToString();
                    option.Text = parts[2].ToString();
                }

                options.Add(option);
                idx++;
            }
        }

        private void ProcessAnswers(string fileName, Category category, HtmlNode answersTableNode, Dictionary<string, QuestionType> questionTypeMap)
        {
            var answerNodes = answersTableNode.SelectNodes("tbody//tr//td").ToList();

            foreach (var answerNode in answerNodes)
            {
                Debug.Assert(answerNode.FirstChild.Name == "#text", $"invalid answers format, {fileName}");

                var questionNumber = Regex.Match(answerNode.FirstChild.InnerText, @"\d+\.\d+").Value;

                var answer = new Answer();

                switch (questionTypeMap[questionNumber])
                {
                    case QuestionType.Single1:
                    case QuestionType.Single2:
                        answer.Single = Regex.Match(answerNode.FirstChild.InnerText, "[A-Z]").Value;
                        break;
                    case QuestionType.Multi:
                        answer.Multiple = answerNode.SelectNodes("ul//li").Select(x => x.InnerText.Trim()).ToList();
                        break;
                    case QuestionType.Match:
                        answer.Multiple = answerNode.SelectNodes("ul//li").Select(x => x.InnerText.Trim()).ToList();
                        for (var i = 0; i < answer.Multiple.Count; i++)
                        {
                            var parts = Regex.Match(answer.Multiple[i], @"(\d+)?[ \.\-]+([A-Z]+)").Groups;

                            Debug.Assert(parts.Count == 3, $"invalid answer format, {answer.Multiple[i]}, {fileName}");

                            if (string.IsNullOrEmpty(parts[1].ToString()))
                            {
                                answer.Multiple[i] = $"{i + 1} - {parts[2]}";
                            }
                        }
                        break;
                    default:
                        throw new NotImplementedException($"SubCategoryType, {fileName}");
                }

                category.Answers.Add(questionNumber, answer);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ParseFiles();

            // DEV
            const string scriptTemplate = "<script type=\"text/javascript\" src=\"./{0}\"></script>";
            const string resultFileName = "Testai-dev.html";

            File.Copy($"{TemplatesFolderPath}{JQueryFileName}", $"{OutputFolderPath}{JQueryFileName}", true);

            var data = $"globalData.categories = {JsonConvert.SerializeObject(categories)};";

            File.WriteAllText($"{OutputFolderPath}{CategoriesFileName}", data);

            var content = File.OpenText($"{TemplatesFolderPath}{TestTemplateFileName}").ReadToEnd()
                .Replace("<!--DATA-->", string.Format(scriptTemplate, CategoriesFileName))
                .Replace("<!--jQuery-->", string.Format(scriptTemplate, JQueryFileName))
                .Replace("<!--DATA SYNC DATE-->", GetDataSyncDate());

            File.WriteAllText($"{OutputFolderPath}{resultFileName}", content);

            Process.Start(OutputFolderPath);
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ParseFiles();

            // PROD
            const string scriptTemplate = "<script type=\"text/javascript\">{0}</script>";
            const string resultFileName = "Testai.html";

            var data = $"globalData.categories = {JsonConvert.SerializeObject(categories)};";

            var jQuery = File.OpenText($"{TemplatesFolderPath}{JQueryFileName}").ReadToEnd();

            var content = File.OpenText($"{TemplatesFolderPath}{TestTemplateFileName}").ReadToEnd()
                .Replace("<!--DATA-->", string.Format(scriptTemplate, data))
                .Replace("<!--jQuery-->", string.Format(scriptTemplate, jQuery))
                .Replace("<!--DATA SYNC DATE-->", GetDataSyncDate());

            File.WriteAllText($"{OutputFolderPath}{resultFileName}", content);

            Process.Start(OutputFolderPath);
            this.Close();
        }

        private string GetDataSyncDate()
        {
            var anyTestFileName = Directory.EnumerateFiles(TestFileFolderPath).First();
            return File.GetCreationTime(anyTestFileName).ToString("yyyy-MM-dd");
        }
    }
}
